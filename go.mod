module parallel-dec

go 1.15

require (
	github.com/klauspost/compress v1.11.7
	github.com/pierrec/lz4 v2.6.0+incompatible
	github.com/pierrec/lz4/v4 v4.1.3
	github.com/saracen/go7z v0.0.0-20191010121135-9c09b6bd7fda // indirect
	github.com/saracen/solidblock v0.0.0-20190426153529-45df20abab6f // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/ulikunitz/xz v0.5.9
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
	golang.org/x/sys v0.0.0-20210113181707-4bcb84eeeb78
)
