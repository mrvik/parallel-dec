// +build !windows

package main

import (
	"os"

	"golang.org/x/sys/unix"
)

var otherSignals = []os.Signal{
	unix.SIGTERM,
	unix.SIGHUP,
}
