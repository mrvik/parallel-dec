package main

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"parallel-dec/7z-server"
	"parallel-dec/tar-server"
	"parallel-dec/writer"
	"parallel-dec/zip-server"
	"path"
	"strings"
)

type readerWithAt interface {
	io.Reader
	io.ReaderAt
}

func fillServers(sendTo chan<- writer.WriteOrder, filenames []string, format string, buffered bool) ([]Server, error) {
	var (
		servers = make([]Server, len(filenames))
		err     error
	)

	for i, filename := range filenames {
		servers[i], err = getServerFor(filename, format, sendTo, buffered)
		if err != nil {
			return nil, err
		}
	}

	return servers, nil
}

func getServerFor(filename, format string, sendTo chan<- writer.WriteOrder, buffered bool) (Server, error) {
	var (
		reader readerWithAt
		size   int64
	)

	if filename == "-" {
		log.Info("Reading from stdin")
		var buf bytes.Buffer
		_, err := io.Copy(&buf, os.Stdin)
		if err != nil {
			panic(err)
		}

		reader, size = bytes.NewReader(buf.Bytes()), int64(buf.Len())
	} else {
		if format == "" {
			if strings.Contains(filename, ".tar.") {
				format = fullTarFormat(filename)
			} else {
				format = strings.TrimPrefix(path.Ext(filename), ".")
			}
		}

		f, err := os.Open(filename)
		if err != nil {
			panic(err)
		}

		stat, err := f.Stat()
		if err != nil {
			panic(err)
		}
		reader, size = &fileBuf{bufio.NewReader(f), f, f}, stat.Size()
	}

	var (
		server Server
		err    error
	)

	switch format {
	case "zip":
		server, err = zipserver.NewServer(reader, size, sendTo)
	case "tar", "tar.bz2", "tar.gz", "tar.lzma", "tar.xz", "tar.lz4", "tar.zst", "tar.zlib":
		server = tarserver.NewServer(strings.TrimPrefix(format, "tar."), reader, sendTo)
	case "7z":
		server, err = p7zserver.NewServer(reader, size, sendTo)
	case "":
		panic("Format cannot be empty")
	default:
		panic("Unkown format: " + format)
	}

	return server, err
}

func fullTarFormat(filename string) string {
	spl := strings.Split(filename, ".tar.")
	fm := spl[len(spl)-1]
	return "tar." + fm
}

type fileBuf struct {
	*bufio.Reader
	io.ReaderAt
	io.Closer
}
