package writer

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

var ErrUnknownMode = errors.New("unknown mode in write order")

var (
	serialMutex = new(sync.Mutex)
	log         = logrus.WithField("package", "writer")
)

type Cluster struct {
	WG       *sync.WaitGroup
	DestPath string

	WriteChannel chan WriteOrder
}

type WriteOrder struct {
	Path string
	Mode os.FileMode

	Modified time.Time
	Accessed time.Time

	// If the file is a symlink (Mode must have os.ModeSymlink)
	LinkDest string

	ContentReader io.Reader
}

func NewCluster(ctx context.Context, dest string, workers int) *Cluster {
	if workers < 1 {
		workers = 1
	}

	wc := Cluster{
		WG:       new(sync.WaitGroup),
		DestPath: dest,

		WriteChannel: make(chan WriteOrder, workers*2), //nolint: gomnd
	}

	wc.startWorkers(ctx, workers)

	return &wc
}

func (wc *Cluster) startWorkers(ctx context.Context, num int) {
	wc.WG.Add(num)

	for i := 0; i < num; i++ {
		go wc.worker(ctx, wc.WG)
	}
}

func (wc *Cluster) worker(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()

loop:
	for {
		var (
			order WriteOrder
			ok    bool
		)
		select {
		case <-ctx.Done():
			break loop
		case order, ok = <-wc.WriteChannel:
			if !ok {
				break loop
			}
		}

		if strings.HasPrefix(order.Path, "../") {
			log.Warnf("trimming ../ on %s", order.Path)
			order.Path = trimAllDotDot(order.Path)
		}

		if err := order.Do(ctx, filepath.Join(wc.DestPath, order.Path)); err != nil {
			panic(err)
		}
	}

	log.Info("Exiting worker")
}

// Process the WriteOrder. If the ContentReader is non nil and implements io.Closer, will be closed at the end.
// File will be written into dest. If dest is empty, path will come from wo.Path.
func (wo WriteOrder) Do(ctx context.Context, dest string) error {
	if wo.ContentReader != nil {
		if closer, ok := wo.ContentReader.(io.Closer); ok {
			defer closer.Close()
		}
	}

	if dest == "" {
		dest = wo.Path
	}

	var (
		file *os.File
		err  error
	)

	log.Infof("Writing file %s (Mode %+s)", dest, wo.Mode)

	serialMutex.Lock()
	switch {
	case wo.Mode.IsDir():
		err = os.MkdirAll(dest, wo.Mode|0o100) // Allow exec on dirs
	case (wo.Mode & os.ModeSymlink) == os.ModeSymlink:
		_ = os.MkdirAll(filepath.Dir(dest), 0o700)
		_ = os.Remove(dest) // Avoid error if file exists
		err = os.Symlink(wo.LinkDest, dest)
	case wo.Mode.IsRegular():
		err = os.MkdirAll(filepath.Dir(dest), 0o700) // Mode should be set by other goroutine
		if err != nil {
			break
		}

		file, err = os.OpenFile(dest, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, wo.Mode)
	default:
		err = fmt.Errorf("%w: %s", ErrUnknownMode, wo.Mode.String())
	}
	serialMutex.Unlock()

	if err != nil {
		return fmt.Errorf("cannot open dest file: %w", err)
	}

	if err = ctx.Err(); err != nil {
		return fmt.Errorf("context has error: %w", err)
	}

	if file != nil {
		defer file.Close()

		var buf [4096]byte
		if _, err := io.CopyBuffer(file, wo.ContentReader, buf[:]); err != nil {
			return fmt.Errorf("cannot write content into file: %w", err)
		}
	}

	if wo.Mode&os.ModeSymlink == os.ModeSymlink {
		return nil // We canot do anything more with symlinks
	}

	perms := wo.Mode.Perm()

	if wo.Mode.IsDir() {
		perms |= 0o100
	}

	if err = os.Chmod(dest, perms); err != nil {
		return fmt.Errorf("cannot change file mode: %w", err)
	}

	if !wo.Accessed.IsZero() || !wo.Modified.IsZero() {
		if err = os.Chtimes(dest, wo.Accessed, wo.Modified); err != nil {
			return fmt.Errorf("cannot change time: %w", err)
		}
	}

	return nil
}

func trimAllDotDot(in string) (out string) {
	out = in

	for strings.HasPrefix(out, "../") {
		out = strings.TrimPrefix(out, "../")
	}

	return
}
