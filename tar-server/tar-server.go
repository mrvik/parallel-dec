package tarserver

import (
	"archive/tar"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"parallel-dec/writer"
)

type Server struct {
	reader  *tar.Reader
	closers []io.Closer
	sendTo  chan<- writer.WriteOrder
}

func NewServer(filter string, from io.Reader, writeTo chan<- writer.WriteOrder) Server {
	var (
		closers  []io.Closer
		readFrom = from
	)

	if closer, ok := from.(io.Closer); ok {
		closers = append(closers, closer)
	}

	if filter != "" && filter != "tar" {
		readFrom = GetFilter(filter, from)
		if cl, ok := readFrom.(io.Closer); ok {
			closers = append(closers, cl)
		}
	}

	return Server{
		tar.NewReader(readFrom),
		closers,
		writeTo,
	}
}

func (s Server) Start(ctx context.Context) error {
loop:
	for {
		header, err := s.reader.Next()
		switch {
		case errors.Is(err, io.EOF):
			break loop
		case err == nil:
		default:
			return fmt.Errorf("cannot get next file: %w", err)
		}

		var (
			fileInfo = header.FileInfo()
			content  bytes.Buffer
			order    = writer.WriteOrder{
				Path:          header.Name,
				Mode:          fileInfo.Mode(),
				Modified:      header.ModTime,
				Accessed:      header.AccessTime,
				LinkDest:      header.Linkname,
				ContentReader: &content,
			}
		)

		_, err = io.Copy(&content, s.reader)
		if err != nil {
			return fmt.Errorf("read tar file: %w", err)
		}

		select {
		case s.sendTo <- order:
		case <-ctx.Done():
			break loop
		}
	}

	return nil
}

func (s Server) Close() {
	if s.closers == nil {
		return
	}

	for _, closer := range s.closers {
		_ = closer.Close()
	}
}
