package tarserver

import (
	"compress/bzip2"
	"compress/gzip"
	"compress/zlib"
	"fmt"
	"io"

	"github.com/klauspost/compress/zstd"
	lz4 "github.com/pierrec/lz4/v4"
	"github.com/ulikunitz/xz"
	"github.com/ulikunitz/xz/lzma"
)

var filters = map[string]func(io.Reader) io.Reader{
	"bz2":  bzip2.NewReader,
	"gzip": must(gzipAdapter),
	"lzma": must(lzmaAdapter),
	"xz":   must(xzAdapter),
	"lz4":  must(lz4Adapter),
	"zst":  must(zstdAdapter),
	"zstd": must(zstdAdapter),
	"zlib": must(zlibAdapter),
}

func GetFilter(name string, r io.Reader) io.Reader {
	filter, ok := filters[name]
	if !ok {
		panic(fmt.Sprintf("no filter named %s, we have %+v", name, filters))
	}

	return filter(r)
}

func gzipAdapter(r io.Reader) (io.Reader, error) {
	return gzip.NewReader(r)
}

func lzmaAdapter(r io.Reader) (io.Reader, error) {
	return lzma.NewReader(r)
}

func xzAdapter(r io.Reader) (io.Reader, error) {
	return xz.NewReader(r)
}

func lz4Adapter(r io.Reader) (io.Reader, error) {
	return lz4.NewReader(r), nil
}

func zstdAdapter(r io.Reader) (io.Reader, error) {
	return zstd.NewReader(r)
}

func zlibAdapter(r io.Reader) (io.Reader, error) {
	return zlib.NewReader(r)
}

func must(fn func(io.Reader) (io.Reader, error)) func(io.Reader) io.Reader {
	return func(r io.Reader) io.Reader {
		r, err := fn(r)
		if err != nil {
			panic(err)
		}

		return r
	}
}
