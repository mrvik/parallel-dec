package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"parallel-dec/writer"
	"runtime"

	"golang.org/x/sync/errgroup"

	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("package", "main")

type Server interface {
	Start(context.Context) error
	Close()
}

func main() {
	var (
		buffered bool
		dest     string
		format   string
		workers  int
		cwd, _   = os.Getwd()
	)

	flag.StringVar(&dest, "dest", cwd, "dest folder")
	flag.IntVar(&workers, "workers", runtime.NumCPU(), "num of writer workers")
	flag.StringVar(&format, "format", "", "archive format. Will be autodetected. Mandatory for stdin. Will be applied to all files")
	flag.BoolVar(&buffered, "buffered", false, "use buffered IO for reading instead of direct")
	flag.Parse()
	filenames := flag.Args()

	defer func() {
		if e := recover(); e != nil {
			fmt.Fprintf(os.Stderr, "Panic: %s\n", e)
			os.Exit(1)
		}
	}()

	if len(filenames) == 0 {
		panic("Must specify filenames for compressed files at the end")
	}

	if err := os.MkdirAll(dest, 0700); err != nil {
		panic(err)
	}

	var (
		ctx, cancel = context.WithCancel(context.Background())
		cluster     = writer.NewCluster(ctx, dest, workers)
		err         error
	)

	defer cancel()

	servers, err := fillServers(cluster.WriteChannel, filenames, format, buffered)
	if err != nil {
		panic(err)
	}

	defer func() {
		for _, server := range servers {
			server.Close()
		}
	}()

	go watchSignals(ctx, cancel)

	group, gctx := errgroup.WithContext(ctx)

	for _, server := range servers {
		srv := server
		group.Go(func() error {
			return srv.Start(gctx)
		})
	}

	if err = group.Wait(); err != nil {
		panic(err)
	}

	log.Info("Done with readers, closing writer channel")

	close(cluster.WriteChannel)

	log.Info("Waiting for remaining workers to finish")
	cluster.WG.Wait()
}

func watchSignals(ctx context.Context, cancel context.CancelFunc) {
	defer cancel()

	channel := make(chan os.Signal, 4) //nolint: gomnd
	signal.Notify(channel, append(otherSignals, os.Interrupt)...)
	defer signal.Stop(channel)

	select { // Both channels will exit
	case <-channel:
	case <-ctx.Done():
	}
}
