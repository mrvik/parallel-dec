package zipserver

import (
	"archive/zip"
	"context"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"parallel-dec/writer"

	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("package", "zip-server")

type Server struct {
	reader *zip.Reader
	sendTo chan<- writer.WriteOrder
	closer io.Closer
}

func NewServer(from io.ReaderAt, size int64, sendTo chan<- writer.WriteOrder) (Server, error) {
	closer, _ := from.(io.Closer)
	reader, err := zip.NewReader(from, size)
	if err != nil {
		err = fmt.Errorf("cannot create reader: %w", err)
	}

	return Server{
		reader: reader,
		sendTo: sendTo,
		closer: closer,
	}, err
}

func (s Server) Start(ctx context.Context) error {
	log.Info("Starting to read zip file")

	for _, file := range s.reader.File {
		var (
			openFile io.ReadCloser
			err      error
			linkDest string
		)

		if file.Name[len(file.Name)-1] != '/' {
			if openFile, err = file.Open(); err != nil {
				return fmt.Errorf("error on zip server: %w", err)
			}
		}

		if (file.Mode() & os.ModeSymlink) == os.ModeSymlink {
			if linkDest, err = readLinkDest(openFile); err != nil {
				return err
			}
		}

		order := writer.WriteOrder{
			Path: file.Name,
			Mode: file.Mode(),

			Modified: file.Modified,
			Accessed: time.Time{}, // Access time is not recoverable

			LinkDest: linkDest,

			ContentReader: openFile,
		}

		select {
		case s.sendTo <- order:
			continue
		case <-ctx.Done():
		}

		break
	}

	return ctx.Err()
}

func readLinkDest(from io.Reader) (string, error) {
	var (
		builder strings.Builder
		err     error
	)

	if _, err = io.Copy(&builder, from); err != nil {
		err = fmt.Errorf("read link target: %w", err)
	}

	return builder.String(), err
}

func (s Server) Close() {
	if s.closer != nil {
		s.closer.Close()
	}
}
