package p7zserver

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	"parallel-dec/writer"

	"github.com/saracen/go7z"
	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("package", "7z-server")

// Default mode. 7zip doesn't store file modes so we have a default here.
const DefaultMode os.FileMode = 0o644

// 7Zip file modes
const (
	modeDir = 0o020
	modeRO  = 0o001
)

type Server struct {
	reader *go7z.Reader
	closer io.Closer

	sendTo chan<- writer.WriteOrder
}

func NewServer(from io.ReaderAt, size int64, writeTo chan<- writer.WriteOrder) (Server, error) {
	log.Warnf("7z file server is experimental, please, double check the results")

	reader, err := go7z.NewReader(from, size)
	if err != nil {
		return Server{}, fmt.Errorf("cannot start 7z reader: %w", err)
	}

	var closer io.Closer

	if v, isCloser := from.(io.Closer); isCloser {
		closer = v
	}

	return Server{
		reader: reader,
		closer: closer,
		sendTo: writeTo,
	}, nil
}

func (s Server) Start(ctx context.Context) error {
	for {
		if err := ctx.Err(); err != nil {
			return fmt.Errorf("canceled: %w", err)
		}

		info, err := s.reader.Next()

		switch {
		case errors.Is(err, io.EOF):
			return nil
		case err != nil:
			return fmt.Errorf("error reading next file: %w", err)
		case info.IsAntiFile: // Files that shouldn't be extracted
			continue
		}

		log.Infof("File %s has mode %#o", info.Name, info.Attrib)

		order := writer.WriteOrder{
			Path: info.Name,
			Mode: fixMode(info.Attrib),

			Modified: info.ModifiedAt,
		}

		buf := new(bytes.Buffer)

		if _, err := io.Copy(buf, s.reader); err != nil {
			return fmt.Errorf("error reading file contents: %w", err)
		}

		if order.Mode&os.ModeSymlink == os.ModeSymlink {
			order.LinkDest = strings.TrimSpace(buf.String())
		}

		order.ContentReader = buf

		select {
		case s.sendTo <- order:
		case <-ctx.Done():
			return fmt.Errorf("canceled: %w", err)
		}
	}
}

func (s Server) Close() {
	if s.closer != nil {
		_ = s.closer.Close()
	}
}

func fixMode(mode uint32) os.FileMode {
	out := DefaultMode

	if mode&modeRO == modeRO { // RO bit
		out &^= 0o222 // Remove write permission
	}

	if mode&modeDir == modeDir { // Directory
		out |= os.ModeDir | 0o100 // Allow exec
	}

	return out
}
